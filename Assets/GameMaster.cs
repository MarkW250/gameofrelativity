﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameMaster : MonoBehaviour
{
    /* STATIC STUFF */
    private static GameMaster _gameMasterSingleton = null;
    public static GameMaster GameMasterSingleton
    {
        get
        {
            return _gameMasterSingleton;
        }
    }


    /* INSTANCE STUFF */
    [Tooltip("Current Frame of Reference")]
    [SerializeField]
    private RefFrame _currentRefFrame = null;

    [Tooltip("Time Text")]
    [SerializeField]
    private Text _timeText = null;

    private float _currentTime = 0.0f;

    public RefFrame CurrentObserver {  get { return _currentRefFrame; } }

    public void OnTimeSliderChange(float newTime)
    {
        _currentTime = newTime;
        _currentRefFrame.Time = newTime;
        UpdateTimeText();

        RefFrame[] refFrames = FindObjectsOfType(typeof(RefFrame)) as RefFrame[];
        foreach (RefFrame frame in refFrames)
        {
            frame.RefreshPosition();
        }
    }


    private void UpdateTimeText()
    {
        string newTimeText = string.Format("t = {0:f2}", _currentTime);
        _timeText.text = newTimeText;
    }

    // Use this for initialization
    void Awake()
    {
        if (_gameMasterSingleton == null)
        {
            DontDestroyOnLoad(gameObject);
            _gameMasterSingleton = this;
        }
        else if (_gameMasterSingleton != this)
        {
            Destroy(gameObject);
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
