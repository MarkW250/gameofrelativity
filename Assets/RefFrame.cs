﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RefFrame : MonoBehaviour
{

    [Tooltip("Time Text")]
    [SerializeField]
    private Text _timeText = null;

    [Tooltip("Position Text")]
    [SerializeField]
    private Text _positionText = null;

    [Tooltip("Velocity Text")]
    [SerializeField]
    private Text _velocityText = null;

    private float _myTime = 0.00f;
    private float _currentVelocity = 0.0f;
    private float _currentGamma = 1.0f;

    public float Velocity { get { return _currentVelocity; } }
    public float Time { get { return _myTime; } set { _myTime = value; } }

    public void RefreshPosition()
    {
        float time = GameMaster.GameMasterSingleton.CurrentObserver.Time;
        float newXPos = time * _currentVelocity;
        transform.position = new Vector2(newXPos, transform.position.y);
        _myTime = time / _currentGamma;

        UpdateVelText();
        UpdateTimeText();
        UpdatePosText();

    }

    public void OnVelocitySliderChange(float newVel)
    {
        float observerVelocity = GameMaster.GameMasterSingleton.CurrentObserver.Velocity;
        _currentVelocity = newVel - observerVelocity;
        _currentGamma = calcGamma(_currentVelocity);
        RefreshPosition();
    }

    private void UpdateVelText()
    {
        if (_velocityText != null)
        {
            string newVelText = string.Format("v = {0:f2}c", _currentVelocity);
            _velocityText.text = newVelText;
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        float _currentGamma = calcGamma(_currentVelocity);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void UpdateTimeText()
    {
        if (_timeText != null)
        {
            string newTimeText = string.Format("t = {0:f2}", _myTime);
            _timeText.text = newTimeText;
        }
    }

    private void UpdatePosText()
    {
        if (_positionText != null)
        {
            float observerX = GameMaster.GameMasterSingleton.CurrentObserver.transform.position.x;
            float myPos = transform.position.x - GameMaster.GameMasterSingleton.CurrentObserver.transform.position.x;
            string newText = string.Format("x = {0:f2}", transform.position.x);

            _positionText.text = newText;
        }
    }


    private float calcGamma(float velocity)
    {
        float gamma = 1 - velocity * velocity;
        gamma = Mathf.Sqrt(gamma);
        gamma = 1.0f / gamma;

        return gamma;
    }

}
